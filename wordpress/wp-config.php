<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'bdd_wp' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', 'root' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J+r*Lm-WlbI<MzpNV+.3<!tS8$jYI|^P>k]92uS]B[amW)i%|3bUY u(Q;ua3mnJ' );
define( 'SECURE_AUTH_KEY',  '7e6H;M[0W206N ]vi2+xN$CQu>M.W.hm6xms1o(-/k-`84CU{nIaVQ7p7*zXm5.b' );
define( 'LOGGED_IN_KEY',    '5fuT7C~E.fP(ZW8@^56fw{+2<@/yz,}[KIm]J-_Bk-s{IuFN)u.Akz+Z*9}`~2>u' );
define( 'NONCE_KEY',        'lAK*p<)N$GpwWA?^)%PJ{/>ljG]>31C6:n%` ikH>(44AJN9nj.3_84o|bh&h+R8' );
define( 'AUTH_SALT',        'ab+.6cOw1![*]m)Gk%Bj*JYY}heNLoV p<1UL|;M{)xm:Z[ <8:p^msH&`X<,sa8' );
define( 'SECURE_AUTH_SALT', ':FxQl/R9UxBm|64=?}JMsD9jVXS`2~dHfWu+6KU5Pis5=%az`+Il`0b<.tS_C_hL' );
define( 'LOGGED_IN_SALT',   '-N;/kG(~C;+*)HGMCX8$2VsKO=Ip::D}#QrHr7xFwoVq>3nxnUhm/l87][~_~8M/' );
define( 'NONCE_SALT',       'g&V)cGbkqcboo4W:x=^mr*T_$|4&mRrh}FXDQ0H4oh*X?(b3-FoqlJfm^O`g9T!8' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
